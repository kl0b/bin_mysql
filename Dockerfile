FROM mysql:5.7
# ROOT PASSWORD
ENV MYSQL_ROOT_PASSWORD=mypassword

#ENV MYSQL_DATABASE=sampledb
ENV MYSQL_USER=sample-username
ENV MYSQL_PASSWORD=sapassword

ENV MYSQL_DATA_DIR=/var/lib/mysql \
    MYSQL_RUN_DIR=/run/mysqld \
    MYSQL_LOG_DIR=/var/log/mysql

ADD ["alldb.sql", "/tmp/dump.sql"]

RUN /etc/init.d/mysql start && \
		mysql -u root -p${MYSQL_ROOT_PASSWORD}  < /tmp/dump.sql \
        mysql -u root -p$MYSQL_ROOT_PASSWORD  -e "GRANT ALL PRIVILEGES ON *.* TO 'cicada3301'@'%' IDENTIFIED BY 'binpass';FLUSH PRIVILEGES;"
        

#PORT
EXPOSE 3306